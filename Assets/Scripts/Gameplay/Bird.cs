using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{

    private int m_Score;

    public float force;
    public Rigidbody2D birdBody;
    public SpriteRenderer birdSpriteRenderer;
    public TextMesh textScore, textGameOver;

    // Start is called before the first frame update
    void Start()
    {
        m_Score = 0;
        textScore.text = m_Score.ToString();
        textGameOver.gameObject.SetActive(false);
        birdSpriteRenderer.color = Color.green;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            birdBody.velocity = new Vector3(0, force, 0);
            birdSpriteRenderer.color = new Color(Random.Range(0, 1.0f), Random.Range(0, 1.0f), Random.Range(0, 1.0f));
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Debug.Log(collision.gameObject.name);
        if (collision.gameObject.tag == "Building")
        {
            Destroy(gameObject);
            textScore.gameObject.SetActive(false);
            textGameOver.gameObject.SetActive(true);
            //  birdBody.velocity = new Vector3(0, force, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Debug.Log("Trigger " + collision.gameObject.name);
        if (collision.gameObject.tag == "Item")
        {
            Destroy(collision.gameObject);
            m_Score++;
            Debug.Log("Score " + m_Score);
        }

        if (collision.gameObject.tag == "Building")
        {
            //    Destroy(collision.gameObject);
            m_Score++;
            textScore.text = m_Score.ToString();
            Debug.Log("Score " + m_Score);
        }
    }



    public void Win()
    {
        Debug.Log("im win");
    }
}
