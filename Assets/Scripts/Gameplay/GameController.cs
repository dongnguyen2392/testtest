using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public float force;


    public Bird bird;
    public Rigidbody birdBody;

    // Start is called before the first frame update
    void Start()
    {


        bird.Win();
        Debug.Log(bird.name + ": " + bird.transform.position);
        // bird.speed = 10;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            birdBody.velocity = new Vector3(0, force, 0);
        }
    }
}
