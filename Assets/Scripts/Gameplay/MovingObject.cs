using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        RandomPosition();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= new Vector3(speed, 0, 0);
        if (transform.position.x < -6)
        {
            Vector3 m_CurrentPosition = transform.position;
            m_CurrentPosition.x = 6;
            transform.position = m_CurrentPosition;

            RandomPosition();
        }
    }

    void RandomPosition()
    {
        Vector3 m_CurrentPosition = transform.position;
        m_CurrentPosition.y = Random.Range(-1.0f, 4.0f); //Random -7 -4
        transform.position = m_CurrentPosition;
    }
}
